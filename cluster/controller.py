from flask import Flask
import main

app = Flask(__name__)

@app.route('/<username>', methods=['GET'])
def index(username):
    print(username)
    return "Hello, %s!" % main.getUserCluster(username)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=4567)
