import matplotlib.pyplot as plt
import names
import sklearn
import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler, LabelEncoder


def getUserCluster(username):
    return getCluster(username)


def getCluster(username):
    raw_data = initDataset()
    data = raw_data.copy()
    scaled_features = preprosData(data)
    kmeans = clusteringUsers(scaled_features)
    cluster_map = getClusterMap(raw_data, kmeans)

    return cluster_map.loc[cluster_map['name'] == username, 'cluster'].values[0]


def getClusterMap(raw_data, kmeans):
    cluster_map = pd.DataFrame()
    print(raw_data.name)
    cluster_map['name'] = raw_data.name
    cluster_map['cluster'] = kmeans.labels_
    print(cluster_map)
    return cluster_map


def clusteringUsers(scaled_features):
    print("Clastering:")
    kmeans = KMeans(
        init="random",
        n_clusters=3,
        n_init=10,
        max_iter=300,
        random_state=42
    )
    #
    kmeans.fit(scaled_features)
    centers = kmeans.cluster_centers_
    print("Centers")
    print(centers)
    print(kmeans.labels_[:5])
    plt.scatter(centers[:, 0], centers[:, 1], c='blue', s=100, alpha=0.9);
    plt.show()

    print(scaled_features)
    return kmeans


def preprosData(data):
    le = LabelEncoder()
    data["name"] = le.fit_transform(data.name)
    print(data.head(10))

    scaler = StandardScaler()  # розшир діапазон значень
    scaled_features = scaler.fit_transform(data)
    print(scaled_features[:10])
    return scaled_features


def initDataset():
    # GENERATE DATASET
    raw_data = pd.DataFrame(np.random.randint(2, size=(100, 4)), columns=['sex', 'hasPet', 'article', 'buy'])
    usernames = [names.get_first_name() for i in range(100)]
    usernames[97] = "Tester"
    usernames[98] = "Roman"
    usernames[99] = "Kutylo"
    raw_data['name'] = usernames
    return raw_data

